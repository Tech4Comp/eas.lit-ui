# EAs.LiT Frontend

This repository holds the EAs.LiT Frontend, which is a NPM project and based on NodeJS, Vue.js, vue-router, vuex, bootstrap-vue,  Webpack, etc. It provides a single page application (SPA) and accesses other EAs.LiT services via web requests.

## How to get started
1. Clone the repository and all sub repositories via `git clone --recursive ...`.
2. Change into the directory and install all dependencies via `npm install`
3. Transpile and run the project via `npm start`. The built SPA should be automatically opened in your browser. If this isn't the case, navigate to [http://localhost:8080](http://localhost:8080)

The fronted is now started in development mode and hot-reloads in case any code was changed. You may run `npm run lint` to lint all code via ESLint.

By default, the frontend is started without any user-management. You may uncomment the OAuth settings inside `src/config.js` or exchange these for your own ones, to start the frontend with user-management enabled. Actually, the frontend doesn't manage any users, but uses the user information if available and prevents to access any projects without a user having logged in.

In case no upstream services were configured inside `src/config.js` or the configured ones are unavailable, the frontend is pretty much useless. So make sure to have these services deployed and configured.

## Production Environment
Either use the included dockerfile to build a docker image and the included docker-compose.yml files to start it up, or run `npm run build` to build the project locally. The latter one results in a `dist` directory, which needs to be served via a web server.

Look at the dockerfile and docker-compose.yml files to gain insights into manual deployment.

## Automatic Builds and Deployments
By including `[build]` to a commit message on the main branch, a GitLab CI pipeline is triggered and builds a docker image, which is published on GitLab. Afterwards, a deployment stage is triggered, which deploys this newly built image to the staging environment. Deployment may also be triggered on its own, by including `[deploy]` to a commit message.

## Development
All SPA related code resides in the `src` directory and is separated into different folders via the default Vue.js project structure (MVC). The easiest way to get around is to look at a desired view and to look for components of this view. Some views are are reusable by providing different parameters to them. Look at the router file to gain insights into reused routes and provided parameters.
