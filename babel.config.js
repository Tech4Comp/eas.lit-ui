module.exports = {
  presets: [
    ['@babel/preset-env'],
    ['@vue/babel-preset-app'],
    ['@babel/preset-react']
  ],
  plugins: [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/plugin-proposal-class-properties'],
    ['@babel/plugin-proposal-private-methods'],
    ['@babel/plugin-syntax-jsx'],
    ['@babel/plugin-transform-react-jsx'],
    ['@babel/plugin-syntax-dynamic-import'],
    ['@babel/plugin-transform-runtime'],
    ['wildcard', {
      exts: ['md'],
      noModifyCase: true
    }]
  ]
}
