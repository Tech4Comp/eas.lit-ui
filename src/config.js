const itemService = {
  uri: 'http://localhost:3000'
  // uri: 'https://item.easlit.erzw.uni-leipzig.de'
}
const sparqlEndpoint = {
  uri: 'http://localhost:3030/eal',
  knowledgeMaps: 'http://localhost:3030/knowledgeMaps'
  // uri: 'https://quit.easlit.erzw.uni-leipzig.de/eal',
  // knowledgeMaps: 'https://quit.easlit.erzw.uni-leipzig.de/knowledgeMaps'
}

const formatService = {
  uri: 'https://localhost:8080'
  // uri: 'https://import.easlit.erzw.uni-leipzig.de'
}

const knowledgeMapService = {
  uri: 'http://localhost:3002'
  // uri: 'https://wu.easlit.erzw.uni-leipzig.de'
}

const examService = {
  uri: 'http://localhost:3003'
  // uri: 'https://ub.easlit.erzw.uni-leipzig.de'
}

const logService = {
  uri: 'http://localhost:3004'
  // uri: 'https://legacy.easlit.erzw.uni-leipzig.de'
}

const dskgWebApp = {
  uri: 'http://localhost:3005'
  // uri: 'https://export.easlit.erzw.uni-leipzig.de'
}

const demoMode = false

const oAuthProvider = {
  // authority: 'https://auth.las2peer.org/auth/realms/main',
  // client_id: 'a4b3f15a-eaec-489a-af08-1dc9cf57347e',
  // response_type: 'id_token token',
  // scope: 'openid profile'
}

export { itemService, sparqlEndpoint, formatService, knowledgeMapService, examService, oAuthProvider, logService, dskgWebApp, demoMode }
