import * as React from 'react' // eslint-disable-line
import { Component } from 'react'

const Ontodia = require('ontodia')

const CLASS_NAME = 'ontodia-group-template'

export class CustomStandardTemplate extends Component {
  render () {
    return (
      <Ontodia.AuthoredEntity templateProps={this.props}>
        {context => this.renderTemplate(context)}
      </Ontodia.AuthoredEntity>
    )
  }

  renderTemplate (context) {
    const { color, types, isExpanded } = this.props
    const label = this.getLabel()
    const pinnedProperties = this.findPinnedProperties(context)

    return (
      <div className={CLASS_NAME}>
        <div className={`${CLASS_NAME}__wrap`} style={{ backgroundColor: color, borderColor: color, minWidth: 'max-content' }}>
          <div className={`${CLASS_NAME}__type-line`} title={label} style={{ marginTop: '0.2rem' }}>
            <div title={types} className={`${CLASS_NAME}__type-line-text-container`} style={{ minWidth: 'max-content' }}>
              <div className={`${CLASS_NAME}__type-line-text`}>
                {types}
              </div>
            </div>
          </div>
          <div className={`${CLASS_NAME}__body`} style={{ borderColor: color, display: 'inline-block', width: '100%', fontSize: '1.5rem', borderRadius: isExpanded ? '0rem' : '', borderBottomWidth: 'rem' }}>
            <span>{this.renderThumbnail()}</span>&nbsp;
            <span className={`${CLASS_NAME}__label`} title={label}>
              {label}
            </span>
            {pinnedProperties
              ? (
              <div className={`${CLASS_NAME}__pinned-props`} style={{ borderColor: color }}>
                {this.renderProperties(pinnedProperties)}
              </div>
                )
              : null}
          </div>
          {isExpanded
            ? (
            <div>
              <hr className={`${CLASS_NAME}__hr`} style={{ backgroundColor: 'white', margin: '0', borderColor: 'darkgrey' }}/>
              <div className={`${CLASS_NAME}__dropdown`} style={{ backgroundColor: 'white', borderColor: color, borderRadius: '0rem 0rem 0.6rem 0.6rem', padding: '0.5rem 0.5rem 0.5rem 0.5rem' }}>
                {this.renderPhoto()}
                <div className={`${CLASS_NAME}__dropdown-content`} style={{ maxWidth: '25rem' }}>
                  {this.renderIri()}
                  {this.renderProperties()}
                </div>
              </div>
            </div>
              )
            : null}
        </div>
      </div>
    )
  }

  findPinnedProperties (context) {
    const { isExpanded, propsAsList, elementId } = this.props
    if (isExpanded) return undefined
    const templateState = context.view.model.getElement(elementId).elementState
    if (!templateState) return undefined
    const pinned = templateState[Ontodia.TemplateProperties.PinnedProperties]
    if (!pinned) return undefined
    const filtered = propsAsList.filter(prop => Boolean(pinned[prop.id]))
    return filtered.length === 0 ? undefined : filtered
  }

  renderProperties () {
    const { propsAsList } = this.props

    if (!propsAsList.length)
      return <div>no properties</div>

    return (
      <div className={`${CLASS_NAME}__properties`}>
        {propsAsList.map(({ name, id, property }) => (
          <div key={id} className={`${CLASS_NAME}__properties-row`}>
            <div className={`${CLASS_NAME}__properties-key`} title={`${name} (${id})`}>
              <strong>{name}:</strong>
            </div>
            <div className={`${CLASS_NAME}__properties-values`}>
              {property.values.map(({ text }, index) => (
                <div className={`${CLASS_NAME}__properties-value`} key={index} title={text}>
                  {text}
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    )
  }

  renderPhoto () {
    const { color, imgUrl } = this.props

    if (!imgUrl) return null

    return (
      <div className={`${CLASS_NAME}__photo`} style={{ borderColor: color }}>
        <img src={imgUrl} className={`${CLASS_NAME}__photo-image`} />
      </div>
    )
  }

  renderIri () {
    const { iri } = this.props
    return (
      <div>
        <div className={`${CLASS_NAME}__iri`}>
          <div className={`${CLASS_NAME}__iri-key`}>
            <strong>IRI:</strong>
          </div>
          <div className={`${CLASS_NAME}__iri-value`}>
            {iri.startsWith('sparql-blank:')
              ? <span>(blank node)</span>
              : <a href={iri} title={iri}>{iri}</a>}
          </div>
        </div>
        <hr className={`${CLASS_NAME}__hr`} style={{ marginTop: '0.5rem', marginBottom: '0.5rem' }}/>
      </div>
    )
  }

  renderThumbnail () {
    const { imgUrl, iconUrl } = this.props

    if (imgUrl) {
      return (
        <div className={`${CLASS_NAME}__thumbnail`} aria-hidden='true'>
          <img src={imgUrl} className={`${CLASS_NAME}__thumbnail-image`} />
        </div>
      )
    } else if (iconUrl) {
      return (
        <span className={`${CLASS_NAME}__thumbnail`} aria-hidden='true'>
          <img src={iconUrl} style={{ maxWidth: '2.2rem' }}/>
        </span>
      )
    } else return ''
  }

  getLabel () {
    const { label, props } = this.props
    return this.getProperty(props, 'http://xmlns.com/foaf/0.1/name') || label
  }

  getProperty (props, id) {
    if (props && props[id])
      return props[id].values.map(v => v.text).join(', ')
    else return undefined
  }
}
