import ky from 'ky'
import isEmpty from 'lodash/isEmpty'
const { sparqlEndpoint, knowledgeMapService } = require('../config')
const jsonld = require('jsonld')

const context = {
  topics: {
    '@id': 'http://tech4comp/eal/linkedTerm',
    '@type': '@id',
    '@container': '@set'
  },
  knowledgeMap: {
    '@id': 'http://tech4comp/eal/linkedKnowledgeMap',
    '@type': '@id'
  },
  element: {
    '@id': 'http://tech4comp/eal/Element',
    '@type': '@id'
  },
  project: {
    '@id': 'http://tech4comp/eal/hasProject',
    '@type': '@id'
  }
}

export default {
  async getAvailableMaps () {
    const query = `
    CONSTRUCT { ?s ?p ?o }
    WHERE {
      ?s ?p ?o
    }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: 'application/ld+json' },
        body: query
      }).json()
      if (response['@graph'])
        return response['@graph']
      else
        return [response]
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      return []
    }
  },

  async getWholeMap (graph, reduce = false, format = 'text/turtle') {
    const query = `PREFIX mluo: <http://halle/ontology/>
      CONSTRUCT { ?s ?p ?o } WHERE {
        GRAPH <${graph}> {
          ${reduce ? '?s a mluo:Term .' : ''}
          ?s ?p ?o .
        }
      }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: format },
        body: query
      }).text()

      return response
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      return ''
    }
  },

  async getSubjects (graph) {
    const query = `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      CONSTRUCT { ?s a ?o . ?s rdfs:label ?label . } WHERE {
        GRAPH <${graph}> {
          ?s a ?o ;
             rdfs:label ?label .
        }
        FILTER (!isBlank(?s))
        FILTER (LANG(?label) = "de")
      }`

    try {
      const response = await ky.post(sparqlEndpoint.knowledgeMaps, {
        headers: { 'Content-Type': 'application/sparql-query', Accept: 'application/n-triples' },
        body: query
      }).text()

      return jsonld.compact(await jsonld.fromRDF(response), { '@language': 'de', label: 'http://www.w3.org/2000/01/rdf-schema#label' })
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.log(e)
      return ''
    }
  },

  async writeKowledgeMapLink (data) {
    data['@context'] = context
    try {
      return await ky.put(knowledgeMapService.uri + '/knowledgeMapLink', {
        body: JSON.stringify(data),
        headers: { Accept: 'application/ld+json', 'Content-Type': 'application/ld+json' }
      }).json()
    } catch (e) {
      console.error(e)
      if (!isEmpty(e.error) && !isEmpty(e.error.message))
        return { status: false, message: e.error.message }
      else {
        return { status: false }
      }
    }
  },

  async getLinksForElement (id) {
    try {
      const result = await ky.get(knowledgeMapService.uri + `/knowledgeMapLink/element/${encodeURIComponent(id)}`, {
        headers: { Accept: 'application/ld+json' }
      }).json()

      return await jsonld.flatten(result, context)
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.error(e)
      return {}
    }
  },

  async getLinksForProject (projectID) {
    try {
      const result = await ky.get(knowledgeMapService.uri + `/knowledgeMapLink/project/${encodeURIComponent(projectID)}`, {
        headers: { Accept: 'application/ld+json' }
      }).json()

      return await jsonld.flatten(result, context)
    } catch (e) {
      if (!isEmpty(e.response) && e.response.status !== 404)
        console.error(e)
      return {}
    }
  }
}
