import ky from 'ky'
import isEmpty from 'lodash/isEmpty'
import last from 'lodash/last'
import every from 'lodash/every'
import isObject from 'lodash/isObject'
const { examService, formatService } = require('../config')

export default {

  async postItemsAndGetSessionID (items) {
    if (isEmpty(items))
      return null

    try {
      return await ky.post(examService.uri + '/items', {
        json: { '@context': {}, '@graph': items }
      }).json()
    } catch (e) {
      console.error(e)
      return null
    }
  },

  async getNumberOfPossibleExams (sessionID = null, criteria = {}) {
    if (isEmpty(sessionID) || isEmpty(criteria))
      return null

    try {
      return await ky.post(examService.uri + `/exams/${sessionID}/amountOfPossibleExams`, {
        json: criteria
      }).json()
    } catch (e) {
      console.error(e)
      return null
    }
  },

  requirements: {
    numberOfExamsToCreate: 1,
    differenceBetweenExams: 100,
    fastAlgorithm: false,
    infoMailAddress: ''
  },

  async getExams (sessionID = null, criteria = {}, requirements = {}) {
    if (isEmpty(sessionID) || isEmpty(criteria))
      return null

    try {
      return await ky.post(examService.uri + `/exams/${sessionID}?numberOfExams=${requirements.numberOfExamsToCreate}&email=${requirements.infoMailAddress}&fastCalculation=${requirements.fastAlgorithm}&intersection=${requirements.differenceBetweenExams}&maxTime=${requirements.maxTime}`, {
        json: criteria,
        timeout: false
      }).json()
    } catch (e) {
      const error = await e.response.json()
      if (error.message === 'toFewExamsGenerated' || error.message === 'intersectionViolation')
        throw new Error(error.message)
      else
        throw e
    }
  },

  async getFullExamAs (idList = [], format = 'csv') {
    if (isEmpty(idList) || !every(idList, String))
      throw new Error('unsatisfiable parameters')

    try {
      return await ky.get(formatService.uri + `/export?idList=${idList.map((id) => last(id.split('/'))).toString()}&outputFormat=${format}`, {
        timeout: false
      }).blob()
    } catch (e) {
      if (e.message.includes('NetworkError'))
        console.error('Cannot connect to formatService')
      else
        console.error(e.message)
      throw e
    }
  },

  async convertExam (itemList = [], sourceFormat = 'json', targetFormat = 'csv') {
    if (isEmpty(itemList) || !every(itemList, isObject))
      throw new Error('unsatisfiable parameters')

    const formData = new FormData()
    formData.append('file', new Blob([JSON.stringify(itemList)]))

    try {
      return await ky.post(formatService.uri + `/convert?inputFormat=${sourceFormat}&outputFormat=${targetFormat}`, {
        body: formData,
        timeout: false
      }).blob()
    } catch (e) {
      console.error(e)
      throw e
    }
  }

}
