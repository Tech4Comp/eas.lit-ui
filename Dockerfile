FROM node:18-alpine
LABEL maintainer="Roy Meissner <meissner@informatik.uni-leipzig.de>"

ARG BUILD_ENV=LOCAL
ENV VIRTUAL_PORT=${VIRTUAL_PORT:-80}
ENV DEMO=false

RUN mkdir /nodeApp
WORKDIR /nodeApp

# ---------------- #
#   Installation   #
# ---------------- #

RUN apk add --no-cache gettext && rm -rf /var/cache/apk/*

RUN npm install -g --no-optional --production serve
COPY ./ ./
RUN if [ "$BUILD_ENV" != "CI" ] ; then rm -R node_modules ; npm install; fi
#RUN npm run build # moved to entrypoint script

# ----------------- #
#   Configuration   #
# ----------------- #

# ----------- #
#   Cleanup   #
# ----------- #

#RUN rm -R node_modules && npm cache clean --force

# -------- #
#   Run!   #
# -------- #

#TODO override config.js or read from env
ENTRYPOINT ["./entrypoint.sh"]
CMD serve -s -n -C -l $VIRTUAL_PORT /nodeApp/dist
