#!/bin/sh

env | grep -i SERVICE_

cat /nodeApp/config.js.template | envsubst > ./src/config.js
npm run build
npm prune --production

exec "$@"
